//open -na "Google Chrome" --args --disable-web-security --user-data-dir="/tmp/temporary-chrome-profile"
//"C:\Program Files\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir="C:\TempChromeProfile"

const TOPOJSON_PATH = 'https://d3js.org/world-110m.v1.json';
const MEDALS_CSV_PATH = 'olympic_years_medals_csv.csv';

const isoToNocMapping = {
    "004": ["AFG"], // Afghanistan
    "008": ["ALB"], // Albania
    "010": ["ATA"], // Antarctica
    "012": ["ALG"], // Algeria
    "016": ["ASM"], // American Samoa
    "020": ["AND"], // Andorra
    "024": ["ANG"], // Angola
    "028": ["ANT"], // Antigua and Barbuda
    "031": ["AZE"], // Azerbaijan
    "032": ["ARG"], // Argentina
    "036": ["AUS"], // Australia
    "040": ["AUT"], // Austria
    "044": ["BAH"], // Bahamas
    "048": ["BRN"], // Bahrain
    "050": ["BAN"], // Bangladesh
    "051": ["ARM", "URS"], // Armenia
    "052": ["BRB"], // Barbados
    "056": ["BEL"], // Belgium
    "060": ["BER"], // Bermuda
    "064": ["BHU"], // Bhutan
    "068": ["BOL"], // Bolivia
    "070": ["BIH", "YUG"], // Bosnia and Herzegovina
    "072": ["BOT"], // Botswana
    "074": ["BVT"], // Bouvet Island
    "076": ["BRA"], // Brazil
    "084": ["BLZ"], // Belize
    "086": ["IOT"], // British Indian Ocean Territory
    "090": ["SLB"], // Solomon Islands
    "092": ["VGB"], // Virgin Islands, British
    "096": ["BRU"], // Brunei Darussalam
    "100": ["BUL"], // Bulgaria
    "104": ["MMR"], // Myanmar
    "108": ["BDI"], // Burundi
    "112": ["BLR", "URS"], // Belarus
    "116": ["KHM"], // Cambodia
    "120": ["CMR"], // Cameroon
    "124": ["CAN"], // Canada
    "132": ["CPV"], // Cape Verde
    "136": ["CYM"], // Cayman Islands
    "140": ["CAF"], // Central African Republic
    "144": ["LKA"], // Sri Lanka
    "148": ["TCD"], // Chad
    "152": ["CHI"], // Chile
    "156": ["CHN"], // China
    "158": ["TWN"], // Taiwan, Province of China
    "162": ["CXR"], // Christmas Island
    "166": ["CCK"], // Cocos (Keeling) Islands
    "170": ["COL"], // Colombia
    "174": ["COM"], // Comoros
    "175": ["MYT"], // Mayotte
    "178": ["COG"], // Congo
    "180": ["COD"], // Congo, the Democratic Republic of the
    "184": ["COK"], // Cook Islands
    "188": ["CRI"], // Costa Rica
    "191": ["CRO", "YUG"], // Croatia
    "192": ["CUB"], // Cuba
    "196": ["CYP"], // Cyprus
    "203": ["CZE"], // Czech Republic
    "204": ["BEN"], // Benin
    "208": ["DEN"], // Denmark
    "212": ["DMA"], // Dominica
    "214": ["DOM"], // Dominican Republic
    "218": ["ECU"], // Ecuador
    "222": ["SLV"], // El Salvador
    "226": ["GNQ"], // Equatorial Guinea
    "231": ["ETH"], // Ethiopia
    "232": ["ERI"], // Eritrea
    "233": ["EST", "URS"], // Estonia
    "234": ["FRO"], // Faroe Islands
    "238": ["FLK"], // Falkland Islands (Malvinas)
    "239": ["SGS"], // South Georgia and the South Sandwich Islands
    "242": ["FJI"], // Fiji
    "246": ["FIN"], // Finland
    "248": ["ALA"], // Åland Islands
    "250": ["FRA"], // France
    "254": ["GUF"], // French Guiana
    "258": ["PYF"], // French Polynesia
    "260": ["ATF"], // French Southern Territories
    "262": ["DJI"], // Djibouti
    "266": ["GAB"], // Gabon
    "268": ["GEO", "URS"], // Georgia
    "270": ["GMB"], // Gambia
    "275": ["PSE"], // Palestinian Territory, Occupied
    "276": ["GER"], // Germany
    "288": ["GHA"], // Ghana
    "292": ["GIB"], // Gibraltar
    "296": ["KIR"], // Kiribati
    "300": ["GRE"], // Greece
    "304": ["GRL"], // Greenland
    "308": ["GRD"], // Grenada
    "312": ["GLP"], // Guadeloupe
    "316": ["GUM"], // Guam
    "320": ["GTM"], // Guatemala
    "324": ["GIN"], // Guinea
    "328": ["GUY"], // Guyana
    "332": ["HTI"], // Haiti
    "334": ["HMD"], // Heard Island and McDonald Islands
    "336": ["VAT"], // Holy See (Vatican City State)
    "340": ["HND"], // Honduras
    "344": ["HKG"], // Hong Kong
    "348": ["HUN"], // Hungary
    "352": ["ISL"], // Iceland
    "356": ["IND"], // India
    "360": ["IDN"], // Indonesia
    "364": ["IRI"], // Iran, Islamic Republic of
    "368": ["IRQ"], // Iraq
    "372": ["IRL"], // Ireland
    "376": ["ISR"], // Israel
    "380": ["ITA"], // Italy
    "384": ["CIV"], // Côte d'Ivoire
    "388": ["JAM"], // Jamaica
    "392": ["JPN"], // Japan
    "398": ["KAZ", "URS"], // Kazakhstan
    "400": ["JOR"], // Jordan
    "404": ["KEN"], // Kenya
    "408": ["PRK"], // Korea, Democratic People's Republic of
    "410": ["KOR"], // Korea, Republic of
    "414": ["KWT"], // Kuwait
    "417": ["KGZ", "URS"], // Kyrgyzstan
    "418": ["LAO"], // Lao People's Democratic Republic
    "422": ["LBN"], // Lebanon
    "426": ["LSO"], // Lesotho
    "428": ["LVA", "URS"], // Latvia
    "430": ["LBR"], // Liberia
    "434": ["LBY"], // Libyan Arab Jamahiriya
    "438": ["LIE"], // Liechtenstein
    "440": ["LTU", "URS"], // Lithuania
    "442": ["LUX"], // Luxembourg
    "446": ["MAC"], // Macao
    "450": ["MDG"], // Madagascar
    "454": ["MWI"], // Malawi
    "458": ["MYS"], // Malaysia
    "462": ["MDV"], // Maldives
    "466": ["MLI"], // Mali
    "470": ["MLT"], // Malta
    "474": ["MTQ"], // Martinique
    "478": ["MRT"], // Mauritania
    "480": ["MUS"], // Mauritius
    "484": ["MEX"], // Mexico
    "492": ["MCO"], // Monaco
    "496": ["MNG"], // Mongolia
    "498": ["MDA", "URS"], // Moldova, Republic of
    "499": ["MNE", "YUG"], // Montenegro
    "500": ["MSR"], // Montserrat
    "504": ["MAR"], // Morocco
    "508": ["MOZ"], // Mozambique
    "512": ["OMN"], // Oman
    "516": ["NAM"], // Namibia
    "520": ["NRU"], // Nauru
    "524": ["NPL"], // Nepal
    "528": ["NED"], // Netherlands
    "531": ["CUW"], // Curaçao
    "533": ["ABW"], // Aruba
    "534": ["SXM"], // Sint Maarten (Dutch part)
    "535": ["BES"], // Bonaire, Saint Eustatius and Saba
    "540": ["NCL"], // New Caledonia
    "548": ["VUT"], // Vanuatu
    "554": ["NZL"], // New Zealand
    "558": ["NIC"], // Nicaragua
    "562": ["NIGc"], // Niger
    "566": ["NGR"], // Nigeria
    "570": ["NIU"], // Niue
    "574": ["NFK"], // Norfolk Island
    "578": ["NOR"], // Norway
    "580": ["MNP"], // Northern Mariana Islands
    "581": ["UMI"], // United States Minor Outlying Islands
    "583": ["FSM"], // Micronesia, Federated States of
    "584": ["MHL"], // Marshall Islands
    "585": ["PLW"], // Palau
    "586": ["PAK"], // Pakistan
    "591": ["PAN"], // Panama
    "598": ["PNG"], // Papua New Guinea
    "600": ["PAR"], // Paraguay
    "604": ["PER"], // Peru
    "608": ["PHL"], // Philippines
    "612": ["PCN"], // Pitcairn
    "616": ["POL"], // Poland
    "620": ["POR"], // Portugal
    "624": ["GNB"], // Guinea-Bissau
    "626": ["TLS"], // Timor-Leste
    "630": ["PRI"], // Puerto Rico
    "634": ["QAT"], // Qatar
    "638": ["REU"], // Réunion
    "642": ["ROU"], // Romania
    "643": ["RUS", "URS"], // Russia and Soviet Union
    "646": ["RWA"], // Rwanda
    "652": ["BLM"], // Saint Barthélemy
    "654": ["SHN"], // Saint Helena
    "659": ["KNA"], // Saint Kitts and Nevis
    "660": ["AIA"], // Anguilla
    "662": ["LCA"], // Saint Lucia
    "663": ["MAF"], // Saint Martin (French part)
    "666": ["SPM"], // Saint Pierre and Miquelon
    "670": ["VCT"], // Saint Vincent and the Grenadines
    "674": ["SMR"], // San Marino
    "678": ["STP"], // São Tomé and Príncipe
    "682": ["SAU"], // Saudi Arabia
    "686": ["SEN"], // Senegal
    "688": ["SRB", "YUG"], // Serbia
    "690": ["SYC"], // Seychelles
    "694": ["SLE"], // Sierra Leone
    "702": ["SGP"], // Singapore
    "703": ["SVK"], // Slovakia
    "704": ["VNM"], // Vietnam
    "705": ["SVN", "YUG"], // Slovenia
    "706": ["SOM"], // Somalia
    "710": ["ZAF"], // South Africa
    "716": ["ZWE"], // Zimbabwe
    "724": ["ESP"], // Spain
    "732": ["ESH"], // Western Sahara
    "736": ["SDN"], // Sudan
    "740": ["SUR"], // Suriname
    "744": ["SJM"], // Svalbard and Jan Mayen
    "748": ["SWZ"], // Swaziland
    "752": ["SWE"], // Sweden
    "756": ["SUI"], // Switzerland
    "760": ["SYR"], // Syrian Arab Republic
    "762": ["TJK", "URS"], // Tajikistan
    "764": ["THA"], // Thailand
    "768": ["TGO"], // Togo
    "772": ["TKL"], // Tokelau
    "776": ["TON"], // Tonga
    "780": ["TTO"], // Trinidad and Tobago
    "784": ["ARE"], // United Arab Emirates
    "788": ["TUN"], // Tunisia
    "792": ["TUR"], // Turkey
    "795": ["TKM", "URS"], // Turkmenistan
    "796": ["TCA"], // Turks and Caicos Islands
    "798": ["TUV"], // Tuvalu
    "800": ["UGA"], // Uganda
    "804": ["UKR", "URS"], // Ukraine
    "807": ["MKD", "YUG"], // Macedonia, the former Yugoslav Republic of
    "818": ["EGY"], // Egypt
    "826": ["GBR"], // United Kingdom
    "831": ["GGY"], // Guernsey
    "832": ["JEY"], // Jersey
    "833": ["IMN"], // Isle of Man
    "834": ["TZA"], // Tanzania, United Republic of
    "840": ["USA"], // United States
    "850": ["VIR"], // Virgin Islands, U.S.
    "854": ["BFA"], // Burkina Faso
    "858": ["URU"], // Uruguay
    "860": ["UZB", "URS"], // Uzbekistan
    "862": ["VEN"], // Venezuela
    "876": ["WLF"], // Wallis and Futuna
    "882": ["WSM"], // Samoa
    "887": ["YEM"], // Yemen
    "894": ["ZMB"], // Zambia
    "896": ["KOS"], // Kosovo
};

const svg = d3.select('#map').append('svg')
    .attr('width', 960)
    .attr('height', 600);

const projection = d3.geoMercator()
    .scale(130)
    .translate([480, 300]);

const path = d3.geoPath().projection(projection);

let medalDataByYearAndCountry = new Map();
let currentYear = '1896';

Promise.all([
    d3.json(TOPOJSON_PATH),
    d3.csv(MEDALS_CSV_PATH)
]).then(function ([world, medalsData]) {

    const rolledUpData = d3.rollups(
        medalsData,
        v => {
            const total = v.length;
            return {
                'Gold': v.filter(d => d.Medal === 'Gold').length,
                'Silver': v.filter(d => d.Medal === 'Silver').length,
                'Bronze': v.filter(d => d.Medal === 'Bronze').length,
            };
        },
        d => d.Year,
        d => d.NOC
    );

    medalDataByYearAndCountry = new Map(rolledUpData.map(([year, countries]) => [year, new Map(countries)]));

    const countries = topojson.feature(world, world.objects.countries).features;
    svg.selectAll('path')
        .data(countries)
        .enter().append('path')
        .attr('d', path)
        .attr('fill', '#ccc')
        .attr('stroke', '#ffffff')
        .attr('stroke-width', 0.5)
        .on('click', function (event, d) {
            showMedalDetails(d.id);
        });

    d3.select('#yearSlider').on('input', function () {
        currentYear = this.value;
        updateMapColor(this.value);
        updateYearLabel(this.value);
    });

    updateMapColor(currentYear);
});

function updateMapColor(year) {
    const currentYearData = medalDataByYearAndCountry.get(String(year)) || new Map();
    const maxMedals = 150;

    const colorScale = d3.scaleLinear()
        .domain([1, maxMedals])
        .range(["#fee5d9", "#a50f15"]);

    svg.selectAll('path')
        .attr('fill', d => {
            const nocCodes = isoToNocMapping[d.id] || [];
            let totalMedals = 0;
            nocCodes.forEach(nocCode => {
                const medals = currentYearData.get(nocCode);
                if (medals) {
                    totalMedals += medals.Gold + medals.Silver + medals.Bronze;
                }
            });
            return totalMedals > 0 ? colorScale(totalMedals) : '#ccc';
        });
}
function showMedalDetails(isoCode) {
    const nocCodes = isoToNocMapping[isoCode] || [];
    const yearData = medalDataByYearAndCountry.get(currentYear) || new Map();
    let totalGold = 0, totalSilver = 0, totalBronze = 0;

    nocCodes.forEach(nocCode => {
        const medals = yearData.get(nocCode) || { Gold: 0, Silver: 0, Bronze: 0 };
        totalGold += medals.Gold;
        totalSilver += medals.Silver;
        totalBronze += medals.Bronze;
    });

    const medalCountsDiv = document.getElementById('counts');
    medalCountsDiv.innerHTML = `
        <p><strong>Gold Medals:</strong> ${totalGold}</p>
        <p><strong>Silver Medals:</strong> ${totalSilver}</p>
        <p><strong>Bronze Medals:</strong> ${totalBronze}</p>`;
}

document.getElementById('playButton').addEventListener('click', function () {
    const button = this;
    if (button.disabled) return;

    button.disabled = true;

    let currentYear = parseInt(document.getElementById('yearSlider').value);
    const endYear = 2016;

    const interval = setInterval(() => {
        if (currentYear > endYear) {
            clearInterval(interval);
            button.disabled = false;
        } else {
            document.getElementById('yearSlider').value = currentYear;
            updateMapColor(currentYear);
            updateYearLabel(currentYear);
            currentYear += 4;
        }
    }, 500);
});


function updateYearLabel(value) {
    document.getElementById('yearLabel').textContent = value;
}


